from django.urls import path
from .views import index, data_buku

app_name = 'searchbook'

urlpatterns = [
    path('', index, name='index'),
    path('data_buku/', data_buku , name='data_buku'),
]
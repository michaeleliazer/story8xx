from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'story8.html')

def data_buku(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    konten = requests.get(url)

    data = json.loads(konten.content)
    return JsonResponse(data, safe=False)
from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import index, data_buku

# Create your tests here.
class story8UnitTest(TestCase):
    def test_url_response_code(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_view_dan_template(self):
        response = self.client.get('/')
        found = resolve('/')
        self.assertEqual(found.view_name, "searchbook:index")
        self.assertTemplateUsed(response, 'story8.html')

    def test_isi_html(self):
        request = HttpRequest()
        response = index(request)
        story8 = response.content.decode('utf 8')
        self.assertIn("Find your book here!!", story8)
